!classDefinition: #NumeroTest category: 'Numero-Exercise'!
TestCase subclass: #NumeroTest
	instanceVariableNames: 'zero one two four oneFifth oneHalf five twoFifth twoTwentyfifth fiveHalfs three eight negativeOne negativeTwo'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:11'!
test01isCeroReturnsTrueWhenAskToZero

	self assert: zero isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:12'!
test02isCeroReturnsFalseWhenAskToOthersButZero

	self deny: one isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test03isOneReturnsTrueWhenAskToOne

	self assert: one isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:13'!
test04isOneReturnsFalseWhenAskToOtherThanOne

	self deny: zero isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:14'!
test05EnteroAddsWithEnteroCorrectly

	self assert: one + one equals: two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:18'!
test06EnteroMultipliesWithEnteroCorrectly

	self assert: two * two equals: four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:20'!
test07EnteroDividesEnteroCorrectly

	self assert: two / two equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:38'!
test08FraccionAddsWithFraccionCorrectly
"
    La suma de fracciones es:
	 
	a/b + c/d = (a.d + c.b) / (b.d)
	 
	SI ESTAN PENSANDO EN LA REDUCCION DE FRACCIONES NO SE PREOCUPEN!!
	TODAVIA NO SE ESTA TESTEANDO ESE CASO
"
	| sevenTenths |

	sevenTenths := (Entero with: 7) / (Entero with: 10).

	self assert: oneFifth + oneHalf equals: sevenTenths! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:52'!
test09FraccionMultipliesWithFraccionCorrectly
"
    La multiplicacion de fracciones es:
	 
	(a/b) * (c/d) = (a.c) / (b.d)
"

	self assert: oneFifth * twoFifth equals: twoTwentyfifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 20:56'!
test10FraccionDividesFraccionCorrectly
"
    La division de fracciones es:
	 
	(a/b) / (c/d) = (a.d) / (b.c)
"

	self assert: oneHalf / oneFifth equals: fiveHalfs! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test11EnteroAddsFraccionCorrectly
"
	Ahora empieza la diversion!!
"

	self assert: one + oneFifth equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:07'!
test12FraccionAddsEnteroCorrectly

	self assert: oneFifth + one equals: (Entero with: 6) / (Entero with: 5)! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:50'!
test13EnteroMultipliesFraccionCorrectly

	self assert: two * oneFifth equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:52'!
test14FraccionMultipliesEnteroCorrectly

	self assert: oneFifth * two equals: twoFifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:57'!
test15EnteroDividesFraccionCorrectly

	self assert: one / twoFifth equals: fiveHalfs  ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 21:59'!
test16FraccionDividesEnteroCorrectly

	self assert: twoFifth / five equals: twoTwentyfifth ! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:38'!
test17AFraccionCanBeEqualToAnEntero

	self assert: two equals: four / two! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:39'!
test18AparentFraccionesAreEqual

	self assert: oneHalf equals: two / four! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:40'!
test19AddingFraccionesCanReturnAnEntero

	self assert: oneHalf + oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test20MultiplyingFraccionesCanReturnAnEntero

	self assert: (two/five) * (five/two) equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:42'!
test21DividingFraccionesCanReturnAnEntero

	self assert: oneHalf / oneHalf equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:43'!
test22DividingEnterosCanReturnAFraccion

	self assert: two / four equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test23CanNotDivideEnteroByZero

	self 
		should: [ one / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:46'!
test24CanNotDivideFraccionByZero

	self 
		should: [ oneHalf / zero ]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Numero canNotDivideByZeroErrorDescription ]
	! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test25AFraccionCanNotBeZero

	self deny: oneHalf isZero! !

!NumeroTest methodsFor: 'tests' stamp: 'HernanWilkinson 5/7/2016 22:50'!
test26AFraccionCanNotBeOne

	self deny: oneHalf isOne! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 4/15/2021 16:45:35'!
test27EnteroSubstractsEnteroCorrectly

	self assert: four - one equals: three! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:47:41'!
test28FraccionSubstractsFraccionCorrectly
	
	self assert: twoFifth - oneFifth equals: oneFifth.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:00'!
test29EnteroSubstractsFraccionCorrectly

	self assert: one - oneHalf equals: oneHalf! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:05'!
test30FraccionSubstractsEnteroCorrectly

	| sixFifth |
	
	sixFifth := (Entero with: 6) / (Entero with: 5).
	
	self assert: sixFifth - one equals: oneFifth! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:08'!
test31SubstractingFraccionesCanReturnAnEntero

	| threeHalfs |
	
	threeHalfs := (Entero with: 3) / (Entero with: 2).
	
	self assert: threeHalfs - oneHalf equals: one.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:48:48'!
test32SubstractingSameEnterosReturnsZero

	self assert: one - one equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:01'!
test33SubstractingSameFraccionesReturnsZero

	self assert: oneHalf - oneHalf equals: zero.! !

!NumeroTest methodsFor: 'tests' stamp: 'HAW 9/24/2018 08:48:14'!
test34SubstractingAHigherValueToANumberReturnsANegativeNumber

	| negativeThreeHalfs |
	
	negativeThreeHalfs := (Entero with: -3) / (Entero with: 2).	

	self assert: one - fiveHalfs equals: negativeThreeHalfs.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:23'!
test35FibonacciZeroIsOne

	self assert: zero fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:32'!
test36FibonacciOneIsOne

	self assert: one fibonacci equals: one! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:39'!
test37FibonacciEnteroReturnsAddingPreviousTwoFibonacciEnteros

	self assert: four fibonacci equals: five.
	self assert: three fibonacci equals: three. 
	self assert: five fibonacci equals: four fibonacci + three fibonacci.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:47'!
test38FibonacciNotDefinedForNegativeNumbers

	self 
		should: [negativeOne fibonacci]
		raise: Error
		withExceptionDo: [ :anError | self assert: anError messageText equals: Entero negativeFibonacciErrorDescription ].! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:49:55'!
test39NegationOfEnteroIsCorrect

	self assert: two negated equals: negativeTwo.
		! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:03'!
test40NegationOfFraccionIsCorrect

	self assert: oneHalf negated equals: negativeOne / two.! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:11'!
test41SignIsCorrectlyAssignedToFractionWithTwoNegatives

	self assert: oneHalf equals: (negativeOne / negativeTwo)! !

!NumeroTest methodsFor: 'tests' stamp: 'NR 9/23/2018 23:50:17'!
test42SignIsCorrectlyAssignedToFractionWithNegativeDivisor

	self assert: oneHalf negated equals: (one / negativeTwo)! !


!NumeroTest methodsFor: 'setup' stamp: 'NR 9/23/2018 23:46:28'!
setUp

	zero := Entero with: 0.
	one := Entero with: 1.
	two := Entero with: 2.
	three:= Entero with: 3.
	four := Entero with: 4.
	five := Entero with: 5.
	eight := Entero with: 8.
	negativeOne := Entero with: -1.
	negativeTwo := Entero with: -2.
	
	oneHalf := one / two.
	oneFifth := one / five.
	twoFifth := two / five.
	twoTwentyfifth := two / (Entero with: 25).
	fiveHalfs := five / two.
	! !


!classDefinition: #Numero category: 'Numero-Exercise'!
Object subclass: #Numero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
* aMultiplier

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
+ anAdder

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 22:21:28'!
- aSubtrahend

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:49'!
/ aDivisor

	self subclassResponsibility ! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/9/2019 16:21:07'!
// aDivisor 
	
	type = #Entero ifTrue:
		[ ^self class with: value // aDivisor integerValue ].
		
	self error: 'Tipo de nmero no soportado'! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/9/2019 16:21:17'!
greatestCommonDivisorWith: anEntero 
	
	type = #Entero ifTrue:
		[^self class with: (value gcd: anEntero integerValue)].
		
	self error: 'Tipo de nmero no soportado'! !

!Numero methodsFor: 'arithmetic operations' stamp: 'HernanWilkinson 5/7/2016 22:48'!
invalidNumberType

	self error: self class invalidNumberTypeErrorDescription! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/23/2018 23:37:13'!
negated
	
	^self * (Entero with: -1)! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/8/2019 21:59:58'!
printOn: aStream
	
	type = #Entero ifTrue:
		[ aStream print: value ].
	
	type = #Fraccion ifTrue:
		[ aStream 
			print: numerator;
			nextPut: $/;
			print: denominator ].! !

!Numero methodsFor: 'arithmetic operations' stamp: 'NR 9/8/2019 21:55:04'!
type
	
	^type! !


!Numero methodsFor: 'testing' stamp: 'NR 9/23/2018 23:36:49'!
isNegative

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isOne

	self subclassResponsibility ! !

!Numero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:49'!
isZero

	self subclassResponsibility ! !


!Numero methodsFor: 'initialization' stamp: 'NR 9/8/2019 20:40:14'!
initializeWith: aValue
	
	type := #Entero.
	value := aValue! !

!Numero methodsFor: 'initialization' stamp: 'NR 9/8/2019 20:40:23'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	type:=#Fraccion.
	numerator := aNumerator.
	denominator := aDenominator ! !


!Numero methodsFor: 'accessing' stamp: 'NR 9/8/2019 20:59:46'!
denominator

	^denominator! !

!Numero methodsFor: 'accessing' stamp: 'NR 9/8/2019 20:33:53'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !

!Numero methodsFor: 'accessing' stamp: 'NR 9/8/2019 20:59:52'!
numerator

	^numerator! !


!Numero methodsFor: 'comparing' stamp: 'NR 9/8/2019 21:50:26'!
= anObject

	^(anObject isKindOf: self class) and: [ type = anObject type and: 
		[ type = #Entero ifTrue: [ value = anObject integerValue] 
		ifFalse: [ type = #Fraccion ifTrue:
			[ (numerator * anObject denominator) = (denominator * anObject numerator) ]]]]! !

!Numero methodsFor: 'comparing' stamp: 'NR 9/8/2019 22:00:49'!
hash
	type = #Entero ifTrue:
		[ ^value hash ].
	type = #Fraccion ifTrue:
		[ ^(numerator hash / denominator hash) hash ].! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Numero class' category: 'Numero-Exercise'!
Numero class
	instanceVariableNames: ''!

!Numero class methodsFor: 'error descriptions' stamp: 'NR 4/15/2021 16:42:02'!
canNotDivideByZeroErrorDescription

	^'No se puede dividir por cero!!!!!!'! !

!Numero class methodsFor: 'error descriptions' stamp: 'ts 9/14/2021 14:45:33'!
invalidNumberTypeErrorDescription
	
	^ 'Tipo de numero invalido!!!!!!'! !


!Numero class methodsFor: 'instance creation' stamp: 'NR 9/8/2019 20:46:40'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	aValue isInteger ifFalse: [  self error: 'aValue debe ser anInteger' ].
	
	^self new initializeWith: aValue! !

!Numero class methodsFor: 'instance creation' stamp: 'NR 9/8/2019 23:20:40'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	aDivisor isZero ifTrue: [ self error: self canNotDivideByZeroErrorDescription ].
	aDividend isZero ifTrue: [ ^aDividend ].
	
	aDivisor isNegative ifTrue: [ ^self with: aDividend negated over: aDivisor negated].
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	denominator isOne ifTrue: [ ^numerator ].

	^self new initializeWith: numerator over: denominator! !


!classDefinition: #Entero category: 'Numero-Exercise'!
Numero subclass: #Entero
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Entero methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 21:05:47'!
* aMultiplier 
	
	^ aMultiplier multiplyEntero: self.
	! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 20:57:15'!
+ anAdder 

	^anAdder addEntero: self.
! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AR 9/16/2021 11:35:24'!
- aSubtrahend 
	
	^ aSubtrahend substractFromEntero: self .! !

!Entero methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 21:27:10'!
/ aDivisor 
	
	^ aDivisor divideEntero: self.! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ts 9/14/2021 16:43:25'!
// aDivisor 
	
	^Entero with: value // aDivisor integerValue! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ts 9/14/2021 16:45:32'!
fibonacci

	| one two |
	
	one := Entero with: 1.
	two := Entero with: 2.	
	
	^ (self - one) fibonacci + (self - two) fibonacci
		! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ts 9/14/2021 16:38:27'!
greatestCommonDivisorWith: anEntero 
	
	^Entero with: (value gcd: anEntero integerValue)! !

!Entero methodsFor: 'arithmetic operations' stamp: 'ts 9/14/2021 16:44:56'!
negated
	
	^self * (Entero with: -1)
	! !


!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 21:01'!
= anObject

	^(anObject isKindOf: self class) and: [ value = anObject integerValue ]! !

!Entero methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:17'!
hash

	^value hash! !


!Entero methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 20:09'!
initalizeWith: aValue 
	
	value := aValue! !

!Entero methodsFor: 'initialization' stamp: 'NR 9/8/2019 22:32:50'!
initializeWith: aValue
	
	value := aValue! !


!Entero methodsFor: 'value' stamp: 'HernanWilkinson 5/7/2016 21:02'!
integerValue

	"Usamos integerValue en vez de value para que no haya problemas con el mensaje value implementado en Object - Hernan"
	
	^value! !


!Entero methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:53:19'!
printOn: aStream

	aStream print: value ! !


!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'ts 9/14/2021 16:13:37'!
addEntero: anAdderEntero
	
	^Entero with: anAdderEntero integerValue + self integerValue.! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/16/2021 15:09:23'!
addFraccion: anAdderFraccion

	^ self * anAdderFraccion denominator + anAdderFraccion numerator / anAdderFraccion denominator.! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:24:22'!
divideEntero: aDividendEntero

	^ Fraccion with: aDividendEntero over: self.! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:38:35'!
divideFraccion: aDividendFraccion

	^ aDividendFraccion numerator / (aDividendFraccion denominator * self) .! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'ts 9/14/2021 16:15:03'!
multiplyEntero: aMultiplierEntero 

	^Entero with: aMultiplierEntero integerValue * self integerValue.! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:18:06'!
multiplyFraccion: aMultplierFraccion

	^ self * aMultplierFraccion numerator / aMultplierFraccion denominator.! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/16/2021 11:37:33'!
substractFromEntero: anEnteroMinuend
	
	^Entero with: anEnteroMinuend integerValue - self integerValue .! !

!Entero methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/16/2021 12:20:32'!
substractFromFraccion: aFraccionMinuend


	| newNumerator newDenominator |

	newNumerator := aFraccionMinuend numerator - (self * aFraccionMinuend denominator).
	newDenominator := aFraccionMinuend denominator.
	
	^newNumerator / newDenominator.! !


!Entero methodsFor: 'testing' stamp: 'NR 9/23/2018 22:17:55'!
isNegative
	
	^value < 0! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:14'!
isOne
	
	^value = 1! !

!Entero methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 20:12'!
isZero
	
	^value = 0! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Entero class' category: 'Numero-Exercise'!
Entero class
	instanceVariableNames: ''!

!Entero class methodsFor: 'instance creation' stamp: 'ts 9/14/2021 14:46:17'!
negativeFibonacciErrorDescription
	^ ' Fibonacci no esta definido aqui para Enteros Negativos!!!!!!'! !

!Entero class methodsFor: 'instance creation' stamp: 'ts 9/15/2021 11:05:24'!
with: aValue 
	
	"Esta verificacion esta puesta por si se equivocan y quieren crear un Entero pasando otra cosa que un Integer - Hernan"
	^(self subclasses
		detect:[:anEnteroSubclass |  aValue isInteger and: [anEnteroSubclass canHandle: aValue]]
		ifNone:[self error: 'aValue debe ser anInteger']) with: aValue! !


!classDefinition: #EnteroCero category: 'Numero-Exercise'!
Entero subclass: #EnteroCero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroCero methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 15:35:06'!
fibonacci

	^Entero with:1! !

!EnteroCero methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 15:55:51'!
initialize

	value:=0! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroCero class' category: 'Numero-Exercise'!
EnteroCero class
	instanceVariableNames: ''!

!EnteroCero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:08:19'!
canHandle: aValue

	^aValue = 0.! !

!EnteroCero class methodsFor: 'as yet unclassified' stamp: 'ts 9/15/2021 11:04:19'!
with: anEntero

	^self new initialize! !


!classDefinition: #EnteroMayorAUno category: 'Numero-Exercise'!
Entero subclass: #EnteroMayorAUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroMayorAUno class' category: 'Numero-Exercise'!
EnteroMayorAUno class
	instanceVariableNames: ''!

!EnteroMayorAUno class methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 17:12:40'!
canHandle: aValue

	^aValue > 1! !

!EnteroMayorAUno class methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 17:09:37'!
with: aPositiveEntero

	^self new initializeWith: aPositiveEntero! !


!classDefinition: #EnteroNegativo category: 'Numero-Exercise'!
Entero subclass: #EnteroNegativo
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroNegativo methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 16:26:22'!
fibonacci

	self error: Entero negativeFibonacciErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroNegativo class' category: 'Numero-Exercise'!
EnteroNegativo class
	instanceVariableNames: ''!

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:08:07'!
canHandle: aValue

	^aValue < 0.! !

!EnteroNegativo class methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 16:34:24'!
with: anEnteroNegativo

	^self new initializeWith: anEnteroNegativo! !


!classDefinition: #EnteroUno category: 'Numero-Exercise'!
Entero subclass: #EnteroUno
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!EnteroUno methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 15:34:34'!
fibonacci

	^Entero with:1! !

!EnteroUno methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 15:56:14'!
initialize

	value:=1! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'EnteroUno class' category: 'Numero-Exercise'!
EnteroUno class
	instanceVariableNames: ''!

!EnteroUno class methodsFor: 'as yet unclassified' stamp: 'ts 9/14/2021 17:13:26'!
canHandle: aValue

	^aValue = 1! !

!EnteroUno class methodsFor: 'as yet unclassified' stamp: 'ts 9/15/2021 11:03:52'!
with: anEntero

	^self new initialize! !


!classDefinition: #Fraccion category: 'Numero-Exercise'!
Numero subclass: #Fraccion
	instanceVariableNames: 'numerator denominator'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:11:44'!
addEntero: anAdderEntero

	| newNumerator newDenominator |

	newNumerator := denominator * anAdderEntero + numerator.
	newDenominator := denominator.
	
	^newNumerator / newDenominator ! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:03:50'!
addFraccion: aFraccion

	| newNumerator newDenominator |

	newNumerator := (numerator * aFraccion denominator) + (denominator * aFraccion numerator).
	newDenominator := denominator * aFraccion denominator.
	
	^newNumerator / newDenominator ! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:26:20'!
divideEntero: aDividendEntero

	^ denominator * aDividendEntero / numerator.! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:28:58'!
divideFraccion: aDividendFraccion

	^ (aDividendFraccion numerator * self denominator) / (aDividendFraccion denominator * self numerator )! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:13:50'!
multiplyEntero: aMultiplierEntero
	
	^numerator * aMultiplierEntero / denominator.! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/13/2021 21:17:09'!
multiplyFraccion: aMultplierFraccion

	^ (numerator * aMultplierFraccion numerator) / (denominator * aMultplierFraccion denominator).! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/16/2021 11:39:23'!
substractFromEntero: anEnteroMinuend

	| newNumerator newDenominator |

	newNumerator := denominator * anEnteroMinuend - numerator.
	newDenominator := denominator.
	
	^newNumerator / newDenominator ! !

!Fraccion methodsFor: 'auxiliary arithmetic operations' stamp: 'AR 9/16/2021 11:51:53'!
substractFromFraccion: aFraccionMinuend

	| newNumerator newDenominator |

	newNumerator := (denominator * aFraccionMinuend numerator) - (numerator * aFraccionMinuend denominator).
	newDenominator := denominator * aFraccionMinuend denominator.
	
	^newNumerator / newDenominator! !


!Fraccion methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 21:14:50'!
* aMultiplier 
	
	^ aMultiplier multiplyFraccion: self.
	! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 21:04:10'!
+ anAdder 

	^anAdder addFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AR 9/16/2021 11:43:04'!
- aSubtrahend 
	
	^ aSubtrahend substractFromFraccion: self .! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'AR 9/13/2021 21:29:27'!
/ aDivisor 
	
	^ aDivisor divideFraccion: self.! !

!Fraccion methodsFor: 'arithmetic operations' stamp: 'NR 9/8/2019 23:08:35'!
negated
	
	 ^self class with: numerator * (Entero with: -1) over: denominator .! !


!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:42'!
= anObject

	^(anObject isKindOf: self class) and: [ (numerator * anObject denominator) = (denominator * anObject numerator) ]! !

!Fraccion methodsFor: 'comparing' stamp: 'HernanWilkinson 5/7/2016 20:50'!
hash

	^(numerator hash / denominator hash) hash! !


!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
denominator

	^ denominator! !

!Fraccion methodsFor: 'accessing' stamp: 'HernanWilkinson 5/7/2016 21:56'!
numerator

	^ numerator! !


!Fraccion methodsFor: 'initialization' stamp: 'HernanWilkinson 5/7/2016 22:54'!
initializeWith: aNumerator over: aDenominator

	"Estas precondiciones estan por si se comenten errores en la implementacion - Hernan"
	aNumerator isZero ifTrue: [ self error: 'una fraccion no puede ser cero' ].
	aDenominator isOne ifTrue: [ self error: 'una fraccion no puede tener denominador 1 porque sino es un entero' ].
	
	numerator := aNumerator.
	denominator := aDenominator ! !


!Fraccion methodsFor: 'testing' stamp: 'NR 9/23/2018 23:41:38'!
isNegative
	
	^numerator < 0! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isOne
	
	^false! !

!Fraccion methodsFor: 'testing' stamp: 'HernanWilkinson 5/7/2016 22:51'!
isZero
	
	^false! !


!Fraccion methodsFor: 'printing' stamp: 'HAW 9/24/2018 08:54:46'!
printOn: aStream

	aStream 
		print: numerator;
		nextPut: $/;
		print: denominator ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Fraccion class' category: 'Numero-Exercise'!
Fraccion class
	instanceVariableNames: ''!

!Fraccion class methodsFor: 'intance creation' stamp: 'AR 9/16/2021 15:03:19'!
with: aDividend over: aDivisor
	
	^(self subclasses
		detect:[:aFraccionSubclass |  aFraccionSubclass canHandleANumerator: aDividend andDenominator: aDivisor])
		with: aDividend over: aDivisor
	! !


!classDefinition: #FraccionWithDenominatorBiggerThanZero category: 'Numero-Exercise'!
Fraccion subclass: #FraccionWithDenominatorBiggerThanZero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithDenominatorBiggerThanZero class' category: 'Numero-Exercise'!
FraccionWithDenominatorBiggerThanZero class
	instanceVariableNames: ''!

!FraccionWithDenominatorBiggerThanZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:04:59'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDivisor integerValue > 0.! !

!FraccionWithDenominatorBiggerThanZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:53:28'!
with: aDividend over: aDivisor

	| greatestCommonDivisor numerator denominator |
	
	greatestCommonDivisor := aDividend greatestCommonDivisorWith: aDivisor. 
	numerator := aDividend // greatestCommonDivisor.
	denominator := aDivisor // greatestCommonDivisor.
	
	^(self subclasses
		detect:[:aFraccionWithDenominatorBiggertThanZeroSubclass | 
			aFraccionWithDenominatorBiggertThanZeroSubclass canHandleANumerator: numerator andDenominator: denominator]) with: numerator over: denominator.	! !


!classDefinition: #FraccionWithDenominatorBiggerThanOne category: 'Numero-Exercise'!
FraccionWithDenominatorBiggerThanZero subclass: #FraccionWithDenominatorBiggerThanOne
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithDenominatorBiggerThanOne class' category: 'Numero-Exercise'!
FraccionWithDenominatorBiggerThanOne class
	instanceVariableNames: ''!

!FraccionWithDenominatorBiggerThanOne class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:05:21'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDivisor integerValue > 1.! !

!FraccionWithDenominatorBiggerThanOne class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:52:53'!
with: aDividend over: aDivisor

	^self new initializeWith: aDividend over: aDivisor.
! !


!classDefinition: #FraccionWithDenominatorOne category: 'Numero-Exercise'!
FraccionWithDenominatorBiggerThanZero subclass: #FraccionWithDenominatorOne
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithDenominatorOne class' category: 'Numero-Exercise'!
FraccionWithDenominatorOne class
	instanceVariableNames: ''!


!FraccionWithDenominatorOne class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:06:37'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDivisor isOne.
! !

!FraccionWithDenominatorOne class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:54:28'!
with: aDividend over: aDivisor

	^ aDividend.
! !


!classDefinition: #FraccionWithDenominatorZero category: 'Numero-Exercise'!
Fraccion subclass: #FraccionWithDenominatorZero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithDenominatorZero class' category: 'Numero-Exercise'!
FraccionWithDenominatorZero class
	instanceVariableNames: ''!

!FraccionWithDenominatorZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:06:47'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDivisor isZero.
! !

!FraccionWithDenominatorZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:55:29'!
with: aDividend over: aDivisor

	self error: self canNotDivideByZeroErrorDescription! !


!classDefinition: #FraccionWithNegativeDenominator category: 'Numero-Exercise'!
Fraccion subclass: #FraccionWithNegativeDenominator
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithNegativeDenominator class' category: 'Numero-Exercise'!
FraccionWithNegativeDenominator class
	instanceVariableNames: ''!


!FraccionWithNegativeDenominator class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:06:11'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDivisor isNegative.
! !

!FraccionWithNegativeDenominator class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:56:59'!
with: aDividend over: aDivisor

	^ aDividend negated / aDivisor negated.
! !


!classDefinition: #FraccionWithNumeratorZero category: 'Numero-Exercise'!
Fraccion subclass: #FraccionWithNumeratorZero
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numero-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'FraccionWithNumeratorZero class' category: 'Numero-Exercise'!
FraccionWithNumeratorZero class
	instanceVariableNames: ''!

!FraccionWithNumeratorZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 15:06:57'!
canHandleANumerator: aDividend andDenominator: aDivisor

	^ aDividend isZero.
! !

!FraccionWithNumeratorZero class methodsFor: 'as yet unclassified' stamp: 'AR 9/16/2021 14:56:20'!
with: aDividend over: aDivisor
	
	^ aDividend.! !
